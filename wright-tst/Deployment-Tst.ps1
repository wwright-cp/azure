Login-AzureRmAccount

Select-AzureRmSubscription -SubscriptionId "cf94ec0e-f9cc-42fd-86b3-8fb2cd3953e3"

New-AzureRmResourceGroup -Name "rg-vNET-tst" -Location "South Central US"
New-AzureRmResourceGroup -Name "rg-FrontEnd-tst" -Location "South Central US"
New-AzureRmResourceGroup -Name "rg-BackEnd-tst" -Location "South Central US"
New-AzureRmResourceGroup -Name "rg-AD-tst" -Location "South Central US"


#101-vnet-two-subnets
New-AzureRmResourceGroupDeployment -ResourceGroupName "rg-vNET-tst" -TemplateFile .\101-vnet-two-subnets\azuredeploy.json -TemplateParameterFile .\101-vnet-two-subnets\azuredeploy.parameters.json

#101-vm-simple-windows
New-AzureRmResourceGroupDeployment -ResourceGroupName "rg-FrontEnd-tst" -TemplateFile .\101-vm-simple-windows\azuredeploy.json -TemplateParameterFile .\101-vm-simple-windows\azuredeploy.parameters.json